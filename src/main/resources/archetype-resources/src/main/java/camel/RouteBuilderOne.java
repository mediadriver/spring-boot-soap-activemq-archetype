#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.camel;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ${package}.AppProperties;

@Component
public class RouteBuilderOne extends RouteBuilder {

	@Autowired
	private AppProperties properties;

	
	@Override
	public void configure() throws Exception {
		
		onException(Exception.class).handled(true).stop();
		
		from("cxf:bean:service").
		log("LOG RECEIVED MESSAGE").
		to("amq:queue:"+properties.getDestination()).
		log("LOG SENT MESSAGE").
		end();
		
	}

}
