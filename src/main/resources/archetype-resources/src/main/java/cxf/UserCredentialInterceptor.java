#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.cxf;

import java.nio.charset.Charset;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.security.AuthenticationException;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserCredentialInterceptor extends AbstractSoapInterceptor {

	private final static Logger logger = LoggerFactory.getLogger(UserCredentialInterceptor.class);

	private String verificationUsername;

	private String verificationPassword;

	private boolean reportFault = false;

	public UserCredentialInterceptor() {
		super(Phase.PRE_PROTOCOL);
	}

	public void handleMessage(SoapMessage message) throws Fault {

		String name = null;
		String password = null;

		// if you want to read more http header messages, just use get method to obtain from HttpServletRequest.
		HttpServletRequest request = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);
		
		final String authorization = request.getHeader("Authorization");
	    
		if (authorization != null && authorization.startsWith("Basic")) {
	        // Authorization: Basic base64credentials
	        String base64Credentials = authorization.substring("Basic".length()).trim();
	        String credentials = new String(Base64.getDecoder().decode(base64Credentials), Charset.forName("UTF-8"));
	        // credentials = username:password
	        final String[] values = credentials.split(":",2);
	        name = values[0];
	        password= values[1];
		}
	        
		if (name == null || password == null) {
			throw new AuthenticationException("Authentication required but no Authentication header was supplied");
		}

		try {

			if (!(verificationUsername.equals(name)) || !(verificationPassword.equals(password))) {
				throw new Exception("Authentication required please verify credentials and try again!");
			}

		} catch (Exception ex) {
			String errorMessage = "Authentication failed for user " + name + " : " + ex.getMessage();

			if (logger.isErrorEnabled()) {
				logger.error(errorMessage, ex);
			}

			if (reportFault) {
				throw new AuthenticationException(errorMessage);
			} else {
				throw new AuthenticationException("Authentication failed (details can be found in server log)");
			}
		}
	}

	public String getVerificationUsername() {
		return verificationUsername;
	}

	public void setVerificationUsername(String verificationUsername) {
		this.verificationUsername = verificationUsername;
	}

	public String getVerificationPassword() {
		return verificationPassword;
	}

	public void setVerificationPassword(String verificationPassword) {
		this.verificationPassword = verificationPassword;
	}

	public boolean isReportFault() {
		return reportFault;
	}

	public void setReportFault(boolean reportFault) {
		this.reportFault = reportFault;
	}
}