#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AppProperties {

	// CXF parameters
	@Value("${symbol_dollar}{cxf.servlet.context}")
	private String cxfServletContext;

	@Value("${symbol_dollar}{cxf.service.address}")
	private String cxfServiceAddres;

	@Value("${symbol_dollar}{cxf.service.options}")
	private String cxfServiceOptions;

	// WS-Security Endpoint parameters
	@Value("${symbol_dollar}{auth.verification.userName}")
	private String verificationUserName;

	@Value("${symbol_dollar}{auth.verification.password}")
	private String verificationPassword;

	// ActiveMQ Properties
	@Value("${symbol_dollar}{amq.broker.url}")
	private String brokerUrl;

	@Value("${symbol_dollar}{amq.userName}")
	private String userName;

	@Value("${symbol_dollar}{amq.password}")
	private String password;

	@Value("${symbol_dollar}{amq.destination}")
	private String destination;

	public String getCxfServletContext() {
		return cxfServletContext;
	}

	public void setCxfServletContext(String cxfServletContext) {
		this.cxfServletContext = cxfServletContext;
	}

	public String getCxfServiceAddres() {
		return cxfServiceAddres;
	}

	public void setCxfServiceAddres(String cxfServiceAddres) {
		this.cxfServiceAddres = cxfServiceAddres;
	}

	public String getCxfServiceOptions() {
		return cxfServiceOptions;
	}

	public void setCxfServiceOptions(String cxfServiceOptions) {
		this.cxfServiceOptions = cxfServiceOptions;
	}

	public String getVerificationUserName() {
		return verificationUserName;
	}

	public void setVerificationUserName(String verificationUserName) {
		this.verificationUserName = verificationUserName;
	}

	public String getVerificationPassword() {
		return verificationPassword;
	}

	public void setVerificationPassword(String verificationPassword) {
		this.verificationPassword = verificationPassword;
	}

	public String getBrokerUrl() {
		return brokerUrl;
	}

	public void setBrokerUrl(String brokerUrl) {
		this.brokerUrl = brokerUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
